import useNumeralToRomanNumber from './useNumeralToRomanNumber';

import { handleNumeralChange } from './handlers';
const isConvertButtonDisabled = (numeral: string, isError: boolean) => numeral === '' || isError;

import styles from './NumeralsToRomanNumbersForm.module.css';

const NumeralsToRomanNumbersPage = () => {
  const { numeral, setNumeral, error, isError, handleSendButtonClick, romanNumber} = useNumeralToRomanNumber();

  const sendButtonClick = (e: any) => {
    e.preventDefault();
    handleSendButtonClick();
  }

  try {
    return (
      <div>
        <h1 className={styles.numeralFormTitle}>Numeral to roman number</h1>
        <form className={styles.numeralForm}>
          <div>
            <label htmlFor="numeral">Numeral</label>
            <input
              type="text"
              id="numeral"
              value={ numeral }
              onChange={handleNumeralChange(setNumeral)}
            />
          </div>
          <p className={styles.romanNumber}>{romanNumber}</p>
          <p className={styles.error}>{error}</p>
          <button
            type="submit"
            disabled={isConvertButtonDisabled(numeral, isError)}
            onClick={sendButtonClick}
          >
            Convert to roman number
          </button>
        </form>
      </div>
    );
  } catch(error) {
    console.log(error);
    return <div>error</div>
  }
};

export default NumeralsToRomanNumbersPage;