interface IHandleNumeralChange {
  target: {
    value: string;
  };
}
export const handleNumeralChange = (setNumeral: Function) =>
  ({ target: { value } }: IHandleNumeralChange) => setNumeral(value);
