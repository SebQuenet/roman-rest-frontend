import validateAndFormatNumeral from '../validateAndFormatNumeral';

describe('validateAndFormatNumeral test suite', () => {
  it.each(['1', '5', '10', '50', '100', '1000', '3999'])('regular numbers should not throw an error', (numeral) => {
    expect(() => validateAndFormatNumeral(numeral)).not.toThrow();
  });

  it.each(['0', '4000', 'abc', '1.5', '1,5', '1 000', '1,000'])('invalid numbers should throw an error', (numeral) => {
    expect(() => validateAndFormatNumeral(numeral)).toThrow();
  });

  it('should work for 1', () => {
    expect(validateAndFormatNumeral('1')).toBe(1);
  });
  it('should work for 3999', () => {
    expect(validateAndFormatNumeral('3999')).toBe(3999);
  });
});
