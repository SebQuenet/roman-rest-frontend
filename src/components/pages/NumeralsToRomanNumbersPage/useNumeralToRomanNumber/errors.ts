const NUMERAL_TOO_LOW_ERROR = 'Numeral is too low';
const NUMERAL_TOO_HIGH_ERROR = 'Numeral is too high';
const NUMERAL_NOT_A_NUMBER_ERROR = 'Numeral is not a number';
const NUMERAL_NOT_AN_INTEGER_ERROR = 'Numeral is not an integer';

const NumeralTooLowError = new Error(NUMERAL_TOO_LOW_ERROR);
const NumeralTooHighError = new Error(NUMERAL_TOO_HIGH_ERROR);
const NumeralNotANumberError = new Error(NUMERAL_NOT_A_NUMBER_ERROR);
const NumeralNotAnIntegerError = new Error(NUMERAL_NOT_AN_INTEGER_ERROR);

export {
  NumeralTooLowError,
  NumeralTooHighError,
  NumeralNotANumberError,
  NumeralNotAnIntegerError,
};
