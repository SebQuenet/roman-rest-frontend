import { useState, useEffect, useCallback } from 'react';
import validateAndFormatNumeral from './validateAndFormatNumeral';

import { fetchNumeralService } from '../../../../services/numeralService';


const useNumeralsToRomanNumbers = () => {
  const [numeral, setNumeral] = useState('');
  const [romanNumber, setRomanNumber] = useState('');
  const [error, setError] = useState('');
  const [isError, setIsError] = useState(false);
  const [isSending, setIsSending] = useState(false)

  const handleSendButtonClick = useCallback(async () => {
    if (isSending) return;
    setIsSending(true)
    try {
      const receivedRomanNumber = await fetchNumeralService(numeral);
      setRomanNumber(receivedRomanNumber);
    } catch (error: any) {
      setRomanNumber('');
      setError(error.message);
      setIsError(true);
    } finally {
      setIsSending(false);
    }
  }, [numeral, isSending]);

  useEffect(() => {
    if (!numeral) {
      setError('');
      setIsError(false);
      return;
    }

    try {
      validateAndFormatNumeral(numeral);
      setError('');
      setIsError(false);
      return;
    } catch (error) {
      setError((error as Error).message);
      setIsError(true);
      return;
    }
  }, [numeral]);

  return { numeral, setNumeral, romanNumber, error, isError, handleSendButtonClick };
};

export default useNumeralsToRomanNumbers;