const SERVER_ERROR = 'An error occurred while contacting server.';

const fetchNumeralService = async (numeral: string): Promise<string> => {
  try {
    const fetchNumeralServiceResponse = await fetch(`http://localhost:3000/numeral/${numeral}`);
    return fetchNumeralServiceResponse.text();
  }
  catch (error) {
    console.error(error);
    throw new Error(SERVER_ERROR);
  }
};

export { fetchNumeralService };
