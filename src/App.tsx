import { NumeralsToRomanNumbersPage } from './components';

import './App.css'

const App = () => {


  return (
    <div className="App">
      <NumeralsToRomanNumbersPage />
    </div>
  )
}

export default App
